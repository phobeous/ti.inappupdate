// This is a test harness for your module
// You should do something interesting in this harness
// to test out the module and to provide instructions
// to users on how to use it by example.


// open a single window
win.open();


var inAppUpdate = require('es.codecorp.ti.inappupdate');
inAppUpdate.isUpadateAvailable(function(result){
    Ti.API.debug(JSON.stringify(result));
});