/**
 * This is a generated file. Do not edit or your changes will be lost
 */

@interface EsCodecorpTiInappupdateModuleAssets : NSObject {

}

- (NSData *)moduleAsset;
- (NSData *)resolveModuleAsset:(NSString*)path;

@end
