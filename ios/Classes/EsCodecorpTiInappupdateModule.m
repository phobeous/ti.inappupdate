/**
 * ti_inappupdate
 *
 * Created by phobeous
 * Copyright (c) 2019 CodeCorp. All rights reserved.
 */

#import "EsCodecorpTiInappupdateModule.h"
#import "TiBase.h"
#import "TiHost.h"
#import "TiUtils.h"

const int _UPDATE_AVAILABLE = 2;
const int _UPDATE_NOT_AVAILABLE = 1;

@implementation EsCodecorpTiInappupdateModule

#pragma mark Internal

// This is generated for your module, please do not change it
- (id)moduleGUID
{
  return @"5e9ac6e5-2a56-4d82-91ca-d8bd5d1a19a8";
}

// This is generated for your module, please do not change it
- (NSString *)moduleId
{
  return @"es.codecorp.ti.inappupdate";
}

#pragma mark Lifecycle

- (void)startup
{
  // This method is called when the module is first loaded
  // You *must* call the superclass
  [super startup];
  DebugLog(@"[DEBUG] %@ loaded", self);
}

#pragma Public APIs

- (void)isUpdateAvailable:(id)callback
{
  ENSURE_SINGLE_ARG(callback, KrollCallback);

  @try {
    NSDictionary* result;
    NSDictionary* infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString* appID = infoDictionary[@"CFBundleIdentifier"];
    NSURL* url = [NSURL URLWithString:[NSString stringWithFormat:@"http://itunes.apple.com/lookup?bundleId=%@", appID]];
    DebugLog(@"[DEBUG] checking store version from URL %@", url);
    NSData* data = [NSData dataWithContentsOfURL:url];
    NSDictionary* lookup = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];

    if ([lookup[@"resultCount"] integerValue] == 1){
        DebugLog(@"[DEBUG] App info from iTunes %@", lookup);
        NSString* appStoreVersion = lookup[@"results"][0][@"version"];
        NSString* currentVersion = infoDictionary[@"CFBundleShortVersionString"];
      result = @{
        @"updateTypesAllowed": @{
          @"immediate": @NO,
          @"flexible": @NO
          },
        @"packageName": lookup[@"results"][0][@"bundleId"],
        @"updateAvailability": [NSNumber numberWithInt:![appStoreVersion isEqualToString:currentVersion] ? _UPDATE_AVAILABLE : _UPDATE_NOT_AVAILABLE],
        @"availableVersionCode": lookup[@"results"][0][@"version"],
        @"code": @0,
        @"success": @YES
      };
      DebugLog(@"[DEBUG] parsed and ready to invoke callback %@", result);
      return [callback call:@[ result, [NSNull null]] thisObject:nil];
    } else {
      result = @{@"code": @1, @"description": @"Couldn't get info for app from iTunes", @"success": @NO };
      DebugLog(@"[DEBUG] parsed and ready to invoke callback %@", result);
      return [callback call:@[ result, [NSNull null]] thisObject:nil];
    }
  }
  @catch (NSException *ex) {
        NSDictionary* result = @{@"code": @1, @"description":[ex reason], @"success": @NO };
        DebugLog(@"[DEBUG] parsed and ready to invoke callback %@", result);
        return [callback call:@[ result, [NSNull null]] thisObject:nil];
    }
}

MAKE_SYSTEM_PROP(UPDATE_AVAILABLE, _UPDATE_AVAILABLE);
MAKE_SYSTEM_PROP(UPDATE_NOT_AVAILABLE, _UPDATE_NOT_AVAILABLE);
MAKE_SYSTEM_PROP(UNKNOWN, 0);

@end
